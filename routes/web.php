<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('/');
Route::post('/booking','FrontController@book');
Auth::routes();

Route::group(['middleware' => 'auth'], function()
{
    Route::get('/home', 'HomeController@index')->name('home');
    /*-- car management --*/
    Route::get('/car','CarController@index')->name('car');
    Route::get('/carinput','CarController@create');
    Route::post('/carsave','CarController@store');
    Route::get('/carshowall','CarController@ShowAll');
    Route::get('/cardetail/{id}','CarController@ShowById');
    Route::get('/caredit/{id}','CarController@edit');
    Route::post('/carupdate','CarController@update');
    Route::get('/cardelete/{id}','CarController@destroy');
});
Route::get('logout', 'Auth\LoginController@logout');
