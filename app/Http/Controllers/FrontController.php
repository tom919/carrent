<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontController extends Controller
{
    //
    public function index()
    {

        return view('frontpage');
    }
    public function book(Request $request)
    {

        //validation
        $request->validate([
            'type' => 'required',
            'year' => 'required',
            'brand' => 'required',
            'amount' => 'required',
            'startdate' => 'required',
            'enddate' => 'required',
            'cust_name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ],[
            'type.required'=>'Car Type Required',
            'year.required'=>'Car Year Required',
            'brand.required'=>'Car Brand Required',
            'amount.required'=>'Car Amount Required',
            'startdate.required'=>'Start Booking Date Required',
            'enddate.required'=>'End Booking Date Required',
            'cust_name.required'=>'Customer Name Required',
            'email.required'=>'Email Required',
            'phone.required'=>'Phone Required'
        ]);

        $carid=$request->id;
        $type=$request->type;
        $year=$request->year;
        $brand=$request->brand;
        $amount=$request->amount;
        $startdate=$request->startdate;
        $enddate=$request->enddate;
        $custName=$request->cust_name;
        $email=$request->email;
        $phone=$request->phone;
        $price=0;

        //check avability

        $avabilityresults = DB::select("CALL CheckCar(?,?,?,?)",[$type,$year,$brand,$amount]);

        //check price
        if($avabilityresults!=null){
            switch ($type) {
                case "mpv":
                    $price=20000;
                    break;
                case "truck":
                    $price=30000;
                    break;
                case "cuv":
                    $price=3500;
                    break;
                case "suv":
                   $price=1500;
                    break;
                case "pickup":
                    $price=1000;
                    break;
                case "minivan":
                    $price=2800;
                    break;
                case "hatchback":
                    $price=4000;
                    break;
                case "coupe":
                    $price=5000;
                    break;
                default:
                    $price=0;
            }
        }


        //discount apply
        if($year<2010){
            $price=$price-((7/100)*$price);
        }
        if($amount>=2){
            $price=$price-((10/100)*$price);
        }
        $duration=($enddate-$startdate)/86400;
        if($duration>3){
            $price=$price-((5/100)*$price);
        }

        //input transaction
        $results = DB::select("CALL BookCar(?,?,?,?,?,?,?,?)",[$carid,$amount,$price,$enddate,$startdate,$custName,$email,$phone]);

        redirect()->route('/')->with('responsemessage',$results[0]);
    }
}
