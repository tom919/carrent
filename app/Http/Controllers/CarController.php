<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('/car/carindex');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/car/inputcar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'type' => 'required',
            'year' => 'required',
            'brand' => 'required',
            'amount' => 'required'
        ],[
            'type.required'=>'Car Type Required',
            'year.required'=>'Car Year Required',
            'brand.required'=>'Car Brand Required',
            'amount.required'=>'Car Amount Required'
        ]);

        //get data
        $type=$request->input('type');
        $year=$request->input('year');
        $brand=$request->input('brand');
        $amount=$request->input('amount');

        //query
        $results = DB::select( DB::raw("CALL InsertCar('$type','$year','$brand','$amount')") );

        //response
        return redirect()->route('car')->with('responsemessage',$results[0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ShowById($id)
    {

        $results = DB::select("CALL CarDetail(?)",[$id]);
        return view('/car/cardetail',["data"=>$results[0]]);
    }
    /* display all data */
    public function ShowAll(Request $request)
    {

        $limit=10;
        $start=$request->input('start')*$limit;
        if($request->input('search.value'))
            {
                $search=$request->input('search.value');
            }
        else
            {
                $search='';
            }
        $results = DB::select("CALL ShowCarAll(?,?,?)",[$start,$limit,$search] );
        $counter_listing=DB::table('car')->count();
        $counter_filter=ceil($counter_listing/$limit);
        return response()->json(["data"=>$results,"draw"=>$request->input('draw'),"recordsTotal"=>$counter_listing,"recordsFiltered"=>$counter_filter]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $results = DB::select("CALL CarDetail(?)",[$id]);
        return view('/car/caredit',["data"=>$results[0]]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //validation
        $request->validate([
            'type' => 'required',
            'year' => 'required',
            'brand' => 'required',
            'amount' => 'required'
        ],[
            'type.required'=>'Car Type Required',
            'year.required'=>'Car Year Required',
            'brand.required'=>'Car Brand Required',
            'amount.required'=>'Car Amount Required'
        ]);

        $id=$request->id;
        $type=$request->type;
        $year=$request->year;
        $brand=$request->brand;
        $amount=$request->amount;
        $results = DB::select("CALL UpdateCar(?,?,?,?,?)",[$id,$type,$year,$brand,$amount]);
        return redirect()->route('car')->with('responsemessage',$results[0]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $results = DB::select("CALL DeleteCar(?)",[$id]);
        return redirect()->route('car')->with('responsemessage',$results[0]);
    }
}
