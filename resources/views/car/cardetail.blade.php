@extends('layouts.carlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Car Detail</div>

                <div class="card-body">
                    <div class="row">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <td>ID</td>
                            <td>{{$data->id}}</td>
                            </tr>
                            <tr>
                                <td>Type</td>
                                <td>{{$data->type}}</td>
                            </tr>
                            <tr>
                                <td>Year</td>
                                <td>{{$data->year}}</td>
                            </tr>
                            <tr>
                                <td>Brand</td>
                                <td>{{$data->brand}}</td>
                            </tr>
                            <tr>
                                <td>Amount</td>
                                <td>{{$data->amount}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-8"></div>
                        <div class="col-md-2">
                        <a href="{{url('/caredit')}}/{{$data->id}}"><button class="btn btn-md btn-outline-warning"><i class="fa fa-edit"></i> Edit</button></a>
                        </div>
                        <div class="col-md-2 ">

                            <a href="{{url('/cardelete')}}/{{$data->id}}"> <button class="btn btn-md btn-outline-danger"><i class="fa fa-trash"></i> Delete</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
