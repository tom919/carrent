@extends('layouts.carlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">New Car</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                <form action="{{url('/carsave')}}" method="post">
                    @csrf
                        <div class="form-group">
                            <div class="row">
                                <label>Car Type :</label>
                            </div>
                            <div class="row">
                                <select class="form-control" name="type">
                                    <option selected disabled>select car type</option>
                                    <option value="mpv">mpv</option>
                                    <option value="truck">truck</option>
                                    <option value="sedan">sedan</option>
                                    <option value="cuv">cuv</option>
                                    <option value="suv">suv</option>
                                    <option value="pickup">pickup</option>
                                    <option value="minivan">minivan</option>
                                    <option value="hatchback">hatchback</option>
                                    <option value="coupe">coupe</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label>Year:</label>
                            </div>
                            <div class="row">
                                <input type="text" name="year" class="form-control" id="datepicker" placeholder="select production date" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label>Brand:</label>
                            </div>
                            <div class="row">
                                <select class="form-control" name="brand">
                                    <option selected disabled>select brand</option>
                                    <option value="ford">ford</option>
                                    <option value="bmw">bmw</option>
                                    <option value="chevrolet">chevrolet</option>
                                    <option value="honda">honda</option>
                                    <option value="toyota">toyota</option>
                                    <option value="audi">audi</option>
                                    <option value="fiat">fiat</option>
                                    <option value="hyundai">hyundai</option>
                                    <option value="tesla">tesla</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label>Amount:</label>
                            </div>
                            <div class="row">
                                <input type="number" name="amount" class="form-control" placeholder="car amount" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10"></div>
                            <div class="col-md-2 pull-right">
                                <button type="submit" class="btn btn-md btn-outline-secondary">Save</button>
                            </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
