@extends('layouts.carlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Car Management</div>

                <div class="card-body">
                    @if (session('responsemessage'))
                            @foreach (session('responsemessage') as $rsp)
                            <div class="alert alert-success">
                            {{$rsp}}
                            </div>
                            @endforeach
                    @endif

                    <div class="row">
                        <a href="{{url('carinput')}}"><button class="btn btn-outline-secondary">New Car</button></a>
                    </div>

                    <div class="row mt-3">
                        <div class="col">
                            <table id="example"  class="display ui celled table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Type</th>
                                        <th>Year</th>
                                        <th>Brand</th>
                                        <th>Amount</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                    </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
