<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
        <title>Car Rental</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha256-siyOpF/pBWUPgIcQi17TLBkjvNgNQArcmwJB8YvkAgg=" crossorigin="anonymous" />
        <!-- Styles -->
        <style>
            .custom-container{
                margin-left:40%;
                margin-top: 10%;
            }
        </style>
    </head>
    <body>
        <div class="custom-container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Car Rental</h2>
                @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{url('/carbook')}}" method="post">
                        @csrf
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <td>Car Type</td>
                        <td>
                            <select class="form-control" name="type">
                                <option selected disabled>select car type</option>
                                <option value="mpv">mpv</option>
                                <option value="truck">truck</option>
                                <option value="sedan">sedan</option>
                                <option value="cuv">cuv</option>
                                <option value="suv">suv</option>
                                <option value="pickup">pickup</option>
                                <option value="minivan">minivan</option>
                                <option value="hatchback">hatchback</option>
                                <option value="coupe">coupe</option>
                            </select>
                        </td>
                        </tr>
                        <tr>
                            <td>Year</td>
                            <td>
                                <input type="text" name="year" class="form-control" id="datepicker"  placeholder="select production date" />
                            </td>
                        </tr>
                        <tr>
                            <td>Brand</td>
                            <td> <select class="form-control" name="brand">
                                <option selected disabled>select brand</option>
                                <option value="ford">ford</option>
                                <option value="bmw">bmw</option>
                                <option value="chevrolet">chevrolet</option>
                                <option value="honda">honda</option>
                                <option value="toyota">toyota</option>
                                <option value="audi">audi</option>
                                <option value="fiat">fiat</option>
                                <option value="hyundai">hyundai</option>
                                <option value="tesla">tesla</option>
                            </select></td>
                        </tr>
                        <tr>
                            <td>From</td>
                            <td>
                                <input type="text" name="startdate" id="startdate" class="form-control" placeholder="start date" />
                            </td>
                        </tr>
                        <tr>
                            <td>To</td>
                            <td>
                                <input type="text" name="enddate" id="enddate" class="form-control" placeholder="end date" />
                            </td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>
                                <input type="text" name="cust_name" class="form-control" placeholder="name" />
                            </td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>
                                <input type="text" name="email" class="form-control" placeholder="Email" />
                            </td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td>
                                <input type="text" name="phone" class="form-control" placeholder="Phone" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-md btn-outline-secondary">Book Now</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </form>
            </div>
            </div>
        </div>
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" defer integrity="sha256-bqVeqGdJ7h/lYPq6xrPv/YGzMEb6dNxlfiTUHSgRCp8=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
            $("#datepicker").datepicker( {
                format: "yyyy",
                orientation: "bottom",
                viewMode: "years",
                minViewMode: "years",
                changeYear:true
            });
            $("#startdate").datepicker( {
                format: "dd-mm-yyyy",

            });
            $("#enddate").datepicker( {
                format: "dd-mm-yyyy",
            });
    } );
</script>
</html>
